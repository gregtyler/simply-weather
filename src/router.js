import Vue from 'vue';
import VueRouter from 'vue-router';
import PageForecast from './pages/PageForecast.vue';
import PageSearch from './pages/PageSearch.vue';
import PageSettings from './pages/PageSettings.vue';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {path: '/', component: PageSearch, name: 'search'},
    {path: '/settings', component: PageSettings, name: 'settings'},
    {path: '/:locationId', component: PageForecast, props: true, name: 'forecast'}
  ]
});
