import Vue from 'vue';
import store from '../store/index.js';
import router from './router.js';
import toast from './toast.js';

Vue.filter('round', value => Math.round(value));

/** Service worker **/
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('./sw.js').then(reg => {
    reg.addEventListener('updatefound', function() {
      toast('A new version of this application is available. Refresh to update.');
    });
  });
};

/** Analytics **/
const _paq = [];
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
  var u = 'https://analytics.gregtyler.co.uk/';
  _paq.push(['setTrackerUrl', u + 'piwik.php']);
  _paq.push(['setSiteId', '6']);
  const script = document.createElement('script');
  const firstScript = document.getElementsByTagName('script')[0];
  script.type = 'text/javascript';
  script.async = true;
  script.defer = true;
  script.src = u + 'piwik.js';
  firstScript.parentNode.insertBefore(script, firstScript);
})();

/** Start app **/
new Vue({
  el: '#app',
  store,
  router,
  render: (createElement) => createElement('router-view')
});
