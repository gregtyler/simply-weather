import secret from '../secret.json';

const host = 'https://api.openweathermap.org/data/2.5/';

/**
 * Make a call against the OpenWeatherMap API
 * @param {String} path The API call being made
 * @param {Object} params Additional parameters to include
 */
export default function api(path, params) {
  params.appid = secret.API_KEY;

  const paramString = Object.keys(params).map(key => {
    return key + '=' + params[key];
  }).join('&');

  return fetch(host + path + '?' + paramString)
    .then(r => r.json())
    .then(obj => {
      if (typeof obj.cod !== 'undefined' && parseInt(obj.cod, 10) !== 200) {
        throw new Error(obj.message);
      } else {
        return obj;
      }
    });
}
