# Weather forecast
This is a client-side app to help you find the weather forecast for where you
live, using the API from [OpenWeatherMap](http://openweathermap.org/api).

You can [see it in action](https://gregtyler.gitlab.io/simply-weather/) or build
it for yourself using the steps below.

The app uses [Vue](https://vuejs.org/) and [Vuex](https://vuex.vuejs.org/en/)
to handle JavaScript components and state management. It uses
[Rollup](https://rollupjs.org/guide/en) for building,
[Karma](http://karma-runner.github.io/) as a test runner and
[ESLint](https://eslint.org/) for JavaScript linting.

# Installation
Requires:
 - Node (LTS or newer)
 - NPM (comes with Node)

To install dependencies, open a command prompt in the root folder (where this
README is) and enter `npm install`.

Copy the file `secret.example.json` into a new `secret.json` file, and store
an API key from [OpenWeatherMap](https://openweathermap.org/appid).

# Development
Whilst developing, in a command line in the root folder, run `npm run dev`.
This will build the JavaScript file and then automatically rebuild it every
time you change the source code.

You can view the application whilst developing by opening the file `index.html`
in the `public` folder in a web browser.

_Optional:_ To access through an HTTP server, run the command `npx http-server`
in the `public` folder. This will install and run a simple server to
`http://localhost:8080/`.

_Optional:_ Install an ESLint plugin in your text editor of choice to show
linting errors as you type.

# Tests
Unit tests for Vue components are stored in the `tests` folder. To run them,
run `npm test` in the root folder. The test runner will automatically open test
browsers, run the tests in them and report back to the command line.

The test runner will stay open and rerun the tests when you change the source
code.

# Deployment
_This app is automatically deployed through GitLab CI. These instructions are
for hosting it manually._

First, create a production build of the app. This is done by running
`npm run build` in the root folder, which will create a minified,
production-ready copy of the JavaScript bundle file.

You can then deploy the `public` directory to your webhost of choice. **Do not
deploy any other files.** The app runs purely from the few files in `public`.

# Future developments
There are a few areas I'd like to develop further. Firstly, I'd like to add
more tests (as always). For the purpose of demonstration a few tests have been
added but they aren't as in-depth as I'd like, nor do they cover enough of the
application.

The second glaring thing to me is that I've completely misused Vuex stores, due
to time limitations. The actions in `locations.js` should handle all of the
data transformation logic that's currently in `PageForecast.vue`. They should
also then use mutations to add that data to the state, so it can be included
automatically in `PageForecast.vue` (rather than being manually set to
`vm.days`).

Timezones aren't handled well. All dates are GMT, which is helpful for those
of us in the UK looking at UK dates but things get real messy when looking at,
say New York, where clear night time temperatures are advertised as mid-morning.
Times should be offset based on the timezone of the location, but timezones
don't seem to be available through the API, and OpenWeatherMap has this problem
itself.

I optimistically created a `settings` Vuex module for uses to set their
preferred language and temperature format, but there's no way to edit it in the
UI. That would be a helpful, and simple, enhancement.

There are always design improvements to make. The overview details of
temperature and rain for each day look naff, and could do with a visual
indicator. I also haven't added rainfall, wind or temperature details to each
three-hour forecast slot, which is a glaring omission for usefulness.

Finally, I'd like to add a simple service worker so the app can easily be
added to someone's homescreen. Working offline probably isn't a necessity,
since you want weather forecasts to be up-to-date, but creating a custom offline
page and caching for slow networks would be handy.
