/* globals describe, it, expect */
import Vue from 'vue';
import PageSearch from '../src/pages/PageSearch.vue';

/**
 * Build the component and identify its DOM
 */
function getElement() {
  const Constructor = Vue.extend(PageSearch);
  const vm = new Constructor().$mount();
  document.body.appendChild(vm.$el);
  return vm.$el;
}

describe('PageSearch', () => {
  it('contains the header', () => {
    expect(getElement().querySelector('h1').innerText).toEqual('Search');
  });
});
