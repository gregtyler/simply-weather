/* globals describe, it, expect */
import Vue from 'vue';
import ForecastDetails from '../src/components/ForecastDetails.vue';
import store from '../store/index.js';

/**
 * Build the component and identify its DOM
 */
function getElement(propsData) {
  const Constructor = Vue.extend(ForecastDetails);
  const vm = new Constructor({propsData, $store: store});
  vm.$store = store;
  vm.$mount();
  document.body.appendChild(vm.$el);
  return vm.$el;
}

describe('ForecastDetails', () => {
  const forecast = {
    main: {
      temp: 6
    },
    weather: [{
      icon: '10d',
      description: 'rain'
    }],
    name: 'Barcelona',
    sys: {
      country: 'ES'
    }
  };

  it('shows the weather title', () => {
    expect(getElement({forecast}).innerText).toContain('Rain');
  });

  it('shows the weather icon', () => {
    const imageURL = getElement({forecast}).querySelector('img').getAttribute('src');
    expect(imageURL).toEqual('https://openweathermap.org/img/w/10d.png');
  });

  it('shows the city name only when asked', () => {
    expect(getElement({forecast})).not.toContain('Barcelona');
    expect(getElement({forecast, showLocation: true}).innerText).toContain('Barcelona, ES');
  });

  it('uses preferred temperature units', () => {
    const units = store.getters.tempUnits;
    expect(getElement({forecast, showLocation: true}).innerText).toContain('6' + units);
  });
});
