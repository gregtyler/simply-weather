/* eslint-env node */
const commonjs = require('rollup-plugin-commonjs');
const json = require('rollup-plugin-json');
const resolve = require('rollup-plugin-node-resolve');
const replace = require('rollup-plugin-replace');
const vue = require('rollup-plugin-vue');

module.exports = function(config) {
  config.set({
    browsers: ['ChromeHeadless', 'Chrome'],
    files: ['tests/*.test.js'],
    frameworks: ['jasmine'],
    reporters: ['progress'],
    preprocessors: {
      'tests/*.test.js': ['rollup']
    },
    rollupPreprocessor: {
      plugins: [
        replace({
          'process.env.NODE_ENV': JSON.stringify('production')
        }),
        json({
          preferConst: true
        }),
        vue({
          css: true
        }),
        resolve(),
        commonjs()
      ],
      output: {format: 'iife'}
    }
  });
};
