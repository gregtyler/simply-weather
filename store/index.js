import Vue from 'vue';
import Vuex from 'vuex';
import db from './db/weather.js';
import forecast from './modules/forecast.js';
import locations from './modules/locations.js';
import settings from './modules/settings.js';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: true,
  modules: {
    forecast,
    locations,
    settings
  }
});

// Get locations and searchHistory from Dexie
db.locations.toArray()
  .then(locations => {
    locations.forEach(location => {
      store.commit('locations/load', location);
    });

    store.commit('settings/load');
  });

export default store;
