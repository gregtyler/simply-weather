const defaultState = {
  units: 'metric',
  lang: 'en'
};
let user;

if (localStorage.getItem('settings') !== null) {
  user = JSON.parse(localStorage.getItem('settings'));
} else {
  user = defaultState;
  localStorage.setItem('settings', JSON.stringify(defaultState));
}

export default {
  namespaced: true,
  state: {
    isLoaded: false,
    user
  },
  getters: {
    tempUnits: state => {
      const units = state.user.units;
      if (units === 'metric') {
        return '°C';
      } else if (units === 'imperial') {
        return '°F';
      } else {
        return 'K';
      }
    }
  },
  mutations: {
    set(state, {key, value}) {
      state.user[key] = value;

      localStorage.setItem('settings', JSON.stringify(state.user));
    },
    load(state) {
      state.isLoaded = true;
    }
  }
};
