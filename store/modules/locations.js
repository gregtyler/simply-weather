import api from '../../src/api.js';
import db from '../db/weather.js';
import Vue from 'vue';

export default {
  namespaced: true,
  state: [],
  actions: {
    load({state, rootState, dispatch}, locationId) {
      const exists = state.find(l => l.id === locationId);
      if (typeof exists !== 'undefined') {
        return;
      } else {
        const params = {
          id: locationId,
          units: rootState.settings.user.units,
          lang: rootState.settings.user.lang
        };

        return api('group', params).then(cities => {
          return dispatch('loadFromAPI', cities.list[0]);
        });
      }
    },
    search({rootState, state, commit, dispatch}, {q}) {
      const params = {
        q,
        units: rootState.settings.user.units,
        lang: rootState.settings.user.lang
      };

      if (q.length >= 3) {
        return db.searches.where('q').equals(q).first().then(search => {
          if (typeof search !== 'undefined') {
            return search.results.forEach(locationId => {
              commit('load', state.find(location => location.id === locationId));
            });
          } else {
            return api('find', params).then(cities => {
              cities.list.forEach(city => {
                dispatch('loadFromAPI', city);
              });
              commit('saveSearch', {q, results: cities.list.map(city => city.id)});
            });
          }
        });
      }
    },
    searchByLocation({dispatch}, {lat, lng}) {
      return api('weather', {lat, lon: lng}).then(city => {
        return dispatch('loadFromAPI', city);
      });
    },
    favourite({commit}, {locationId, value}) {
      return db.locations.update(locationId, {isFavourite: value ? 1 : 0, updatedAt: new Date()}).then(() => {
        commit('favourite', {locationId, value});
      });
    },
    loadFromAPI({commit}, data) {
      const date = new Date();
      const location = {
        id: data.id,
        name: data.name,
        country: data.sys.country
      };

      return db.locations.get({id: location.id}).then(dbLocation => {
        if (typeof dbLocation !== 'undefined') {
          db.locations.update(location.id, {name: location.name, country: location.country, updatedAt: date});
          commit('load', Object.assign(dbLocation, location));
          return location;
        } else {
          db.locations.put({id: location.id, name: location.name, country: location.country, isFavourite: 0, createdAt: date, updatedAt: date});
          commit('load', location);
          return location;
        }
      });

    }
  },
  mutations: {
    load(state, location) {
      const exists = state.find(l => l.id === location.id);
      if (typeof exists !== 'undefined') {
        Vue.set(state, state.indexOf(exists), location);
      } else {
        state.push(location);
      }
    },
    favourite(state, {locationId, value}) {
      const location = state.find(location => location.id === locationId);
      location.isFavourite = value ? 1 : 0;
    },
    saveSearch(state, {q, results}) {
      db.searches.add({q, results});
    }
  }
};
