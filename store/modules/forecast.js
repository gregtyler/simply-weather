import api from '../../src/api.js';

export default {
  namespaced: true,
  state: {
    locationId: null,
    days: []
  },
  actions: {
    load({rootState, commit}, {locationId}) {
      const params = {
        id: locationId,
        units: rootState.settings.user.units,
        lang: rootState.settings.user.lang
      };

      return api('forecast', params).then(forecast => {
        commit('load', forecast);
      });
    }
  },
  mutations: {
    load(state, data) {
      const today = new Date();

      state.locationId = data.city.id;

      // Collect the data into day objects
      data.list.forEach(entry => {
        const date = entry.dt_txt.substr(0, 10);
        // Identify date for this entry, or create it is necessary
        let dayCollector = state.days.find(d => d.date === date);
        if (!dayCollector) {
          dayCollector = {
            date,
            isToday: new Date(date).toDateString() === today.toDateString(),
            slots: [],
            aggregate: {}
          };
          state.days.push(dayCollector);
        }

        const startDate = new Date(entry.dt * 1000);
        const endDate = new Date(startDate);
        endDate.setHours(endDate.getHours() + 3);

        const slotForecast = {
          start: startDate,
          end: endDate,
          weather: {
            icon: entry.weather[0].icon,
            description: entry.weather[0].description.substr(0, 1).toUpperCase() + entry.weather[0].description.substr(1).toLowerCase()
          },
          temperature: {
            average: entry.main.temp,
            min: entry.main.temp_min,
            max: entry.main.temp_max
          }
        };

        dayCollector.slots.push(slotForecast);
      });

      // Limit to five days
      state.days.splice(5);

      // Calculate the daily aggregates
      state.days.forEach(day => {
        day.aggregate.tempMax = Math.max.apply(this, day.slots.map(e => e.temperature.min));
        day.aggregate.tempMin = Math.min.apply(this, day.slots.map(e => e.temperature.max));
        day.aggregate.rainfall = day.slots.reduce((sum, e) => sum + (e.rain && e.rain['3h'] ? e.rain['3h'] : 0), 0).toFixed(0);

        const weatherGrouped = [];
        day.slots.forEach(slot => {
          const group = weatherGrouped.find(g => g.icon === slot.weather.icon);
          if (typeof group === 'undefined') {
            weatherGrouped.push(Object.assign({count: 1}, slot.weather));
          } else {
            group.count++;
          }
        });

        day.aggregate.weather = weatherGrouped.sort((a, b) => b.count - a.count)[0];
      });
    }
  }
};
