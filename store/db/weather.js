import Dexie from 'dexie';

// Setup database
const db = new Dexie('weather_db');
db.version(1).stores({
  locations: 'id,name,country,isFavourite,createdAt,updatedAt',
  searches: '++id,q,results'
});

export default db;
